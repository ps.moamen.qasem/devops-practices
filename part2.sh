#! /bin/bash 
tag_commit=`git rev-parse --short=8 HEAD`
#tag_commit=$1
cd target
jar_file=`ls *$commitId.jar`
tag_date=$(echo $jar_file | cut -d- -f2)
IMAGE_TAG=${tag_date}-${tag_commit} 
cd ..
echo "IMAGE_TAG="${IMAGE_TAG} > .env
docker build -t moamenqasem/assignment:${IMAGE_TAG} --build-arg jar_tag=${IMAGE_TAG} .
docker push moamenqasem/assignment:${IMAGE_TAG}
