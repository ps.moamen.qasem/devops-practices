FROM openjdk:8-alpine
WORKDIR /proj
ARG jar_tag
ENV jar_tag2=${jar_tag}
ENV SPA=h2
COPY ./target/assignment-${jar_tag2}.jar .
USER root
RUN adduser -D momen  && chmod 777 -R /proj
USER momen
CMD java -jar -Dspring.profiles.active=${SPA} assignment-${jar_tag2}.jar
